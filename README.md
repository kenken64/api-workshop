## Workshop 1

Google static map API key

https://developers.google.com/maps/documentation/maps-static/get-api-key

News API

https://newsapi.org/account

OpenWeather API 

https://home.openweathermap.org/api_keys


## Workshop 3
```
mongoimport --db=cities --collection=cities --file=E:\Projects\restapi-workshop\nodejs\workshop02\zips.json
```

```
import requests

url = 'http://localhost:3000/api/states'
req = requests.get(url)

print(req.status_code)
print(req.headers)
print(req.text)
```

```
import requests

url = 'http://localhost:3000/api/city/01005'
req = requests.get(url)

print(req.status_code)
print(req.headers)
print(req.text)
```

```
import requests

url = 'http://localhost:3000/api/state/WI'
req = requests.get(url)

print(req.status_code)
print(req.headers)
print(req.text)
```

```
import requests

url = 'http://localhost:3000/api/city'
headers = {'Content-Type': 'application/json'}
body = """{ "city_id" : "99951", "city" : "KETCHIKAN2", "loc" : [ -133.18479, 55.942471 ], "pop" : 422, "state" : "AK" }"""

req = requests.post(url, headers=headers, data=body)

print(req.status_code)
print(req.headers)
print(req.text)
```

## Workshop 4
```
npm install apidoc -g
apidoc -f "routes/.*\\.js$" -i ./  -o apidoc/
npm install && npm start
open http://localhost:3000/apidoc
```

start the consul service discovery agent

```
consul agent -dev -ui -node mynode
```

## Worksheet 3

## Worksheet 4
