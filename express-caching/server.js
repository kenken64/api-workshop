const express = require('express');
const cacheControl = require('express-cache-controller');
const preconditions = require('express-preconditions');

const app = express();
app.use(preconditions())

employees = [
    { name: "Kelvin", title: "Accountant", lastModified: new Date()},
    { name: "Kenneth", title: "Technician", lastModified: new Date()},
    { name: "Kenan", title: "Engineer", lastModified: new Date()},
]

app.get('/api/employee/:id',
    //cacheControl({ maxAge: 30, private: true }),
    //preconditions(),
    (req, resp) => {
    let employeeId = req.params.id;
    const result = employees[parseInt(employeeId)];
    resp.setHeader('ETag', new Date(result.lastModified).toGMTString());
    resp.status(200).json(result);
});

app.listen(3000, function () {
  console.log('Appserver started on port 3000');
});