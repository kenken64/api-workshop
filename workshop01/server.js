require('dotenv').config();
const express = require('express'),
    hbs = require('express-handlebars'),
    request = require('request'),
    errors = require('throw.js'),
    winston = require('winston'),
    keys = require('./keys');

const PORT = parseInt(process.env.APP_PORT || 3000);

const WEATHER_API_URL = process.env.WEATHER_API_URL;
const GOOGLE_MAP_API = process.env.GOOGLE_MAP_API;
const NEWS_API_URL = process.env.NEWS_API_URL;

const logger = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    defaultMeta: { service: 'user-service' },
    transports: [
        new winston.transports.File({ filename: 'error.log', level: 'error' }),
        new winston.transports.File({ filename: 'combined.log' })
    ]
});

// change the logging format.
if (process.env.NODE_ENV !== 'production') {
    logger.add(new winston.transports.Console({
        format: winston.format.simple()
    }));
}

/* 
 * Generic make invocation method for calling external API
 */
const makeInvocation = function (url) {
    return ((params) =>
        new Promise((resolve, reject) => {
            request.get(url, ('qs' in params ? params : { qs: params }),
                (err, h, body) => {
                    if (err)
                        return reject(err);
                    if (h.headers['content-type'].startsWith('application/json'))
                        return resolve(JSON.parse(body));
                    resolve(body);
                }
            )
        })
    );
};

// closure
const getWeather = makeInvocation(WEATHER_API_URL);
const getNews = makeInvocation(GOOGLE_MAP_API);
const getMap = makeInvocation(NEWS_API_URL);

const app = express();
app.engine('hbs', hbs());
app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');

app.get('/map', (req, res, next) => {
    // NOTE: boords hold the coordinates for the map
    const coord = {
        lat: parseFloat(req.query.lat),
        lon: parseFloat(req.query.lon),
    }

    //TODO 3/3: Add query parameters for Static Map
    //Use the exact query parameter names as keys
    //Latitude and longitude from coord object above
    //API key is in keys.map
    const params = {
        key: keys.map,
        center: `${coord.lat},${coord.lon}`,
        size: '300x300',
        zoom: 15,
        markers: `size:mid|color:red|label:A|${coord.lat},${coord.lon}`
    }

    getMap({qs: params, encoding : null}).then(result=>{
        res.status(200);
        res.type('image/png');
        res.send(result);
    }).catch(error=>{
        next(new errors.BadRequest());
    })
});

app.get('/information', (req, resp, next)=>{
    //Note: cityName holds the cityName, to be used
     //in params below
     const cityName = req.query.cityName;

    //TODO 1/3: Add query parameters for OpenWeatherMap
    //Use the exact query parameter names as keys
    //Weather for city is in cityName variable
    //API key is in keys.weather
    const params = {
        q: cityName,
        appid: keys.weather
    }

    getWeather(params)
        .then(result => {
			   // NOTE: countryCode holds the 2 character country code
            const countryCode = result.sys.country.toLowerCase();

            //TODO 2/3: Add query parameters for News API
            //Use the exact query parameter names as keys
            //The 2 character country code is found in countryCode variable
            //API key is in keys.news
            const params = {
                country: countryCode,
                category: 'technology',
                apiKey: keys.news
            }
            return (Promise.all([ result, getNews(params) ]));
        })
        .then(result => {
            resp.status(200);
            resp.format({
                'text/html': () => {
                    resp.type('text/html');
                    resp.render('information', {
                        layout: false,
                        city: cityName.toUpperCase(),
                        weather: result[0].weather,
                        temperature: result[0].main,
                        coord: result[0].coord,
                        news: result[1].articles
                    })
                },
                'application/json': () => {
                    const respond = {
                        temperature: result[0].main,
                        coord: result[0].coord,
                        city: cityName,
                        weather: result[0].weather.map(v => {
                            return {
                                main: v.main,
                                description: v.description,
                                icon: `http://openweathermap.org/img/w/${v.icon}.png`
                            }
                        })
                    }
                    resp.json(respond)
                },
                'default': () => {
                    resp.status(406);
                    resp.type('text/plain');
                    resp.send(`Cannot produce the requested representation: ${req.headers['accept']}`);
                }
            })
        })
        .catch(error => {
            resp.status(400); 
            resp.type('text/plain'); 
            resp.send(error); 
            return;
        })
})

app.use((err, req, res, next) => {
    logger.error(err);
    let environment = process.env.NODE_ENV;

    if (environment !== 'development' && environment !== 'test') {
        delete err.stack;
    }

    res.status(err.statusCode || 500).json(err);
});

app.get(/.*/, express.static(__dirname + '/public'));

app.listen(PORT, () => {
    console.info(`Application started on port ${PORT} at ${new Date()}`);
});