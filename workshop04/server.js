const { join } = require('path');
const uuid = require('uuid/v1')
const keys = require('./keys.json');

const CitiesDB = require('./db/citiesdb');
const cors = require('cors');

console.info(`Using ${keys.mongo}`);

// TODO change your databaseName and collectioName 
// if they are not the defaults below
const db = CitiesDB({  
	connectionUrl: keys.mongo, 
	databaseName: 'cities', 
	collectionName: 'cities'
});

const userRouter = require('./routers/user');
const cityRouter = require('./routers/city')(db);

const { ValidationError } = require('express-json-validator-middleware')
const  OpenAPIValidator  = require('express-openapi-validator').OpenApiValidator;

const consul = require('consul')({ promisify: true });

const express = require('express');

require('./db/db');

const serviceId = uuid().substring(0, 8);
const serviceName = `zips`
const auth = require('./middleware/auth')

const app = express();

//Disable etag for this workshop
app.set('etag', false);

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(userRouter);
app.use(cityRouter);


app.use('/apidoc', express.static(join(__dirname, 'apidoc')));

new OpenAPIValidator({ 
    apiSpec: join(__dirname, 'schema', 'app-api.yaml')
}).install(app)


app.get('/health', auth, (req, resp) => {
	console.info(`health check: ${new Date()}`)
	resp.status(200)
		.type('application/json')
		.json({ time: (new Date()).toGMTString() })
})

app.use('/schema', express.static(join(__dirname, 'schema')));

app.use((error, req, resp, next) => {

	if (error instanceof ValidationError) {
		console.error('Schema validation error: ', error)
		return resp.status(400).type('application/json').json({ error: error });
	}

	else if (error.status) {
		console.error('OpenAPI specification error: ', error)
		return resp.status(400).type('application/json').json({ error: error });
	}

	console.error('Error: ', error);
	resp.status(400).type('application/json').json({ error: error });
});

db.getDB()
	.then((db) => {
		const PORT = parseInt(process.argv[2] || process.env.APP_PORT) || 3000;

		console.info('Connected to MongoDB. Starting application');
		app.listen(PORT, () => {
			console.info(`Application started on port ${PORT} at ${new Date()}`);
			console.info(`\tService id: ${serviceId}`);

			// TODO 3/3 Add service registration here
			console.info(`Registering service ${serviceName}:${serviceId}`)
			consul.agent.service.register({
				id: serviceId,
				name: serviceName,
				port: PORT,
				check: {
					ttl: '10s',
					deregistercriticalserviceafter: '30s'
				}
			}).then(() => {
				setInterval(
					() => {
						consul.agent.check.pass({ id: `service:${serviceId}` })
					}, 10000 //10s
				)
				process.on('SIGINT', () => {
					console.info(`Deregistering service ${serviceName}:${serviceId}`)
					consul.agent.service.deregister({ id: serviceId })
						.finally(() => process.exit())
				})
			}).catch(error => console.error('Error: ', error))

		});
	})
	.catch(error => {
		console.error('Cannot connect to mongo: ', error);
		process.exit(1);
	});